﻿(function () {
	'use strict';
	angular.module("xPlat.controllers")
        .controller('ToDoCtrl', ['$scope', 'maps', 'storage', function ($scope, maps, storage) {
            var refresh = function () {
                $scope.todos = storage.getAll();
            }

            var updateAddress = function (toDoItem) {
                return maps.getCurrentPosition()
                    .then(maps.getAddressFromPosition, function (error) { return error.message; })
                    .then(function (address) {
                        toDoItem.address = address;
                        return storage.update(toDoItem);
                    }, function (errorMessage) {
                        toDoItem.address = errorMessage;
                        return storage.update(toDoItem);
                    });
            }

            $scope.addToDo = function () {
                var text = $scope.newToDoText;
                if (!text) {
                    return;
                };

                $scope.newToDoText = '';
                storage.create(text, "Getting location...")
                    .then(function (todo) {
                        $scope.todos.push(todo);
                        return todo;
                    }).then(updateAddress);
            }

            $scope.changeToDoText = function (toDoItem) {
                storage.update(toDoItem)
                    .then(updateAddress)
            }

            $scope.toggleToDoDone = function (toDoItem) {
                toDoItem.done = !toDoItem.done;
                storage.update(toDoItem);
            }

            $scope.removeToDo = function (toDoItem) {
                storage.del(toDoItem).then(function (todo) {
                    var index = $scope.todos.indexOf(todo);
                    $scope.todos.splice(index, 1);
                });
            }
            refresh();
        }])

    .controller('DataLogCtrl', ['$scope', 'mongoFactory', 'cordovaGeolocationService', function ($scope, mongoFactory, cordovaGeolocationService) {
       
        var refreshLogs = function () {
            $scope.logs = mongoFactory.getAll();
            //alert(cordovaGeolocationConstants.apiVersion);   //works..
            //alert(cordovaGeolocationService.apiVersion()); does not work....
             //cordovaGeolocationService.apiVersion;
        }

        $scope.getLog = function () {
            var id = $scope.logIdsearch;
            $scope.logIdsearch = '';
            var retValue = mongoFactory.getLog(id);
            $scope.logs.push(retValue);
        }

        $scope.updateLog = function (dataLog) {
            if (dataLog) {
                mongoFactory.updateLog(dataLog);
            }
        }

        $scope.createLog = function () {
            var vehicleName = $scope.newVehicleName;
            if (!vehicleName) { return; };
            $scope.newVehicleName = '';
            mongoFactory.create(vehicleName).then(function (response) {
                $scope.logs.push(response);
            });
        }

        $scope.removeDataLog = function (dataLog) {
            mongoFactory.removeDataLog(dataLog).then(function (item) {
                var i = $scope.logs.indexOf(item);
                $scope.logs.splice(i, 1);
            });
        
        }

        //currently this call will bit work due to 'use strict' mode I believe
        $scope.getApiVersion = function () {
            alert('current api version: ' + cordovaGeolocationService.cordovaVersion);
        }

        $scope.getCurrentPosition = function () {
            cordovaGeolocationService.getCurrentPosition(function (position) {
                alert(
                    'Latitude: ' + position.coords.latitude + '\n' +
                    'Longitude: ' + position.coords.longitude + '\n' +
                    'Altitude: ' + position.coords.altitude + '\n' +
                    'Accuracy: ' + position.coords.accuracy + '\n' +
                    'Altitude Accuracy: ' + position.coords.altitudeAccuracy + '\n' +
                    'Heading: ' + position.coords.heading + '\n' +
                    'Speed: ' + position.coords.speed + '\n' +
                    'Timestamp: ' + position.timestamp + '\n'
                );
            });
        }

        $scope.startWatching = function () {
            cordovaGeolocationService.watchPosition(function (position) {
                alert(
                    'Latitude: ' + position.coords.latitude + '\n' +
                    'Longitude: ' + position.coords.longitude + '\n' +
                    'Altitude: ' + position.coords.altitude + '\n' +
                    'Accuracy: ' + position.coords.accuracy + '\n' +
                    'Altitude Accuracy: ' + position.coords.altitudeAccuracy + '\n' +
                    'Heading: ' + position.coords.heading + '\n' +
                    'Speed: ' + position.coords.speed + '\n' +
                    'Timestamp: ' + position.timestamp + '\n'
                );
            });
        }

        $scope.clearWatch = function () {
            cordovaGeolocationService.clearWatch(function (watchID) {
                alert('watch id: ' + watchID);
            });
        }


        $scope.checkGeolocationAvailability = function () {
            cordovaGeolocationService.checkGeolocationAvailability(function (availability) {
                alert('Geolocation availability: ' + availability);
            });
        };


        refreshLogs();
    }]);
})();