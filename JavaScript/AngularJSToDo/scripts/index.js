﻿(function() {
    'use strict';
    angular.module('xPlat', ['xPlat.services', 'xPlat.controllers', 'xPlat.directives', 'xPlat.cordovaGeolocationModule']);
    angular.module('xPlat.directives', []);
    angular.module('xPlat.controllers', []);
    angular.module('xPlat.services', ['ngResource']);
    //angular.module('xPlat.cordovaGeolocationModule', []);  //this causes bad things...

})();